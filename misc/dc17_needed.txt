# This file is a list of miscellaneous things that dc17 locals noticed during dc16 and wanted to keep in mind

= Must have =

* Adapteurs électriques internationaux (e.g. en avoir quelques uns dans la salle d'orga qu'on puisse vendre aux personnes qui en auraient pas)
* Café, bouilloire, bodom, tasses...Thé!
* Équipement pour les membres d'orga
** CBs / cartes SIM pour comms
** Clefs (frontdesk, orga room, noc/video)
** imprimante
** plastificateur pour affiches <3
** scanner pour remboursements
* panneau d'annonces mis à jour régulièrement
** prévisions météo
** menu du jour
** transportation times? (or when people are going downtown to buy things)
* des salles plus tranquilles
* observances religieuses (repas) (Kosher/halal/what else?)
* laveuses/sécheuses self-service à la buanderie à côté du cégep (et la possibilité de mingler les undercrackers)
* gaffer tape pour fils qu'on installe par terre
* services d'assitance planifiés d'avance (ex. mobilité réduite, aveugles, etc.)
* 4-5 PC avec Debian pré-installé dessus
* extensions & multiprises en masse
* affichettes & flèches pour s'orienter à l'intérieur
* objets perdus
* make it well known that the venue is a public space and that ppl should not leave their stuff unattended for to prevent theft.
** get one or two extra laptops in case of theft? :p
** know the insurance details, if these are covered, how to proceed...

= Nice to have =

* des sofas...
* service de lavage de linge
* cartes postales à vendre et timbres
* permettre aux speakers de soumettre leurs slides via wafer
* pré-installer des powerbars dans les salles qui auraient besoin
* items variés pour les participants qui oublient:
** personal hygiene: savon, shampooing, serviettes, rasoirs (pour ceux qui ne veulent pas une barbe Debconf)
** feminine hygiene: ...
** personal comfort / security:
*** power plug adapters (maybe offer as swag?)
**** http://www.getdatgadget.com/kikkerland-universal-travel-adapter/
**** https://www.amazon.com/Kikkerland-UL03-A-Universal-Travel-Adapter/dp/B00210MRGC?ie=UTF8&tag=getdatgadget-20
*** cadenas (casiers)
*** bouteilles d'eau etc
* bière sans gluten, bière sans alcool
* un bot irc qui annonce les temps de bouffe pour que personne online oublie (Rhonda faisait celui de DC16)
** Rhonda s'occupe tous les ans de configurer DCschedule
* front desk phone
* attendance certificate
* plus de pad pour le # de tshirts petits et/ou femmes
** (Moray is good at calculating those numbers)
* afficher les dimensions des t-shirts sur la page web du choix
* carte des salles de l'événement (et p-e l'extérieur) au front desk
* trousse de premiers soins pour front desk + acétaminophène + ibuprofène
