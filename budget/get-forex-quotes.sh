#!/bin/sh
set -eu

CURRENCIES="${@:-USD EUR CHF MXN}"

TMPFILE=$(tempfile -p forex)
cleanup() { rm -f $TMPFILE; trap - 0 1 2 3 4 5 6 7 8 10 11 12 13 14 15; }
trap cleanup 0 1 2 3 4 5 6 7 8 10 11 12 13 14 15

DATE=$(TZ=UTC date --rfc-3339=seconds | cut -d+ -f1)
wget --user-agent "Firefox, no really!" -qO $TMPFILE \
  http://www.xe.com/currency/cad-canadian-dollar?c=CHF

for currency in $CURRENCIES; do
  sed -rne "s@.*rel='CAD,${currency}[^>]+>([[:digit:]]+)\.([[:digit:]]+).*@P ${DATE} $currency \1,\2 CAD@p" $TMPFILE
done

cleanup
