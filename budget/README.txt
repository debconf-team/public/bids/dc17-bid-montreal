The easiest way to use this and include currency conversion is to call
./wrapper instead of ledger, e.g.

  ./wrapper bal -X USD

To get the balance sheet in USD. The wrapper script passes a few sensible
default command-line switches, and also updates the currency exchange rates
once per day.

Using the wrapper script, you can also see who spent what if the transactions
have been tagged correctly with this command:

  ./wrapper bal -X USD tag Payer=$yournick

If you want porcelain:

To grab a forex conversion db run...
# ./get-forex-quotes.sh > forex.db

To display the budget in ZAR, just run...
# ledger -f budget.ledger --price-db forex.db balance -X ZAR

To get a difference between the budget and the journal...
# ledger -f journal.ledger --price-db forex.db balance income expenses liabilities budget equity -X ZAR
