commodity USD
   note US dollars
   format USD 1,000.00
   alias $
   nomarket

commodity CAD
   note Canadian dollars
   format CAD 1,000.00
   nomarket
   default

commodity EUR
   note Euros
   format EUR 1,000.00
   nomarket

commodity CHF
   note Swiss francs
   format CHF 1,000.00
   nomarket

commodity MXN
   note Mexican pesos
   format MXN 1,000.00
   nomarket
